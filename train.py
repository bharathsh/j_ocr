from ast import literal_eval

import numpy as np 
import pandas as pd 
path = 'J_OCR/'

# Load dataset with annotations 
annots = pd.read_csv(path+'/annot_4.csv')
annots = pd.concat([annots.drop('region_shape_attributes',1),
                    annots['region_shape_attributes'].apply(literal_eval).apply(pd.Series)],1)
annots =  annots.drop(['file_size', 'file_attributes', 'region_count', 'region_attributes', 'name'],1)
annots['url'] = path+'original/'+annots['#filename']
annots['region_id']+=1

zero = annots[annots['region_id'] == 1].index

# Resize the image maintaining the aspect rato 
def resize_image(image, width=32*18, height = 32*24, to_gray= False):
#   image = cv2.imread(url)
  if to_gray:
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
  if image.shape[1]<image.shape[0]:  
    new_height = height
    new_width  = new_height * image.shape[1] / image.shape[0]
  else: 
    new_width = width
    new_height  = new_width * image.shape[0] / image.shape[1]
  return cv2.resize(image, (int(new_width), int(new_height)))

# Make the image size constant by padding 0's to the image 
def pad_zeros(image,width=(32*18),height = (32*24)):
  result = np.zeros((height,width,3))
  image = image[:height,:width]
  result[:image.shape[0],:image.shape[1]] = image
  return result

# Read image and resize 

import cv2
from google.colab.patches import cv2_imshow

for l in zero:    
  # Read image
  img = cv2.imread(annots['url'][l])
  # Create a mask 
  mask = np.zeros(img.shape[:2],dtype = 'uint8')

  for i in [l,l+1]:
      # Store x and y coordinates 
      c = annots['all_points_x'][i]
      r = annots['all_points_y'][i]
      rid = annots['region_id'][i]
      rc = np.array((c,r)).T
      # Draw contours on mask 
      cv2.drawContours(mask,[rc],0,int(rid),-1)
      # Make mask 3 dimenational 
      
  # Stack the image and mask for visualization
  mask = cv2.cvtColor(mask,cv2.COLOR_GRAY2BGR)
 
  original_url = path+'resized/original/' + annots['#filename'][l].split('.')[0]+'.png'
  mask_url = path+'resized/masks/'+ annots['#filename'][l].split('.')[0]+'.png'
  img = pad_zeros(resize_image(img))
  mask = pad_zeros(resize_image(mask))
  cv2.imwrite(original_url, img)
  cv2.imwrite(mask_url, mask)
  
 #  Image Augmentation 
import os 
import imutils

img = os.listdir(path+'resized/original/')
images = [cv2.imread(path+'resized/original/'+i) for i in img]
masks = [cv2.imread(path+'resized/masks/'+i) for i in img]

org = path+'resized/augmented/original/'
mas = path+'resized/augmented/masks/'
  
def augment(img,mask,n): 
  for i in [100,150,200,250,300,400,500,32*24]:
    image = img[:i]
    ma = mask[:i] 
    for angle in np.arange(0, 180, 15):
      rotated = pad_zeros(resize_image(imutils.rotate_bound(image, angle))) 
      cv2.imwrite(org+ f'{n}_x_{i}_rotated_{angle}.png',rotated)
      rotated = pad_zeros(resize_image(imutils.rotate_bound(ma, angle))) 
      cv2.imwrite(mas+ f'{n}_x_{i}_rotated_{angle}.png',rotated)

  for i in [250,300,32*18]:
    image = img[:,:i]
    ma = mask[:,:i]  
    for angle in np.arange(0, 180, 15):
      rotated = pad_zeros(resize_image(imutils.rotate_bound(image, angle))) 
      cv2.imwrite(org+f'{n}_y_{i}_rotated_{angle}.png',rotated)
      rotated = pad_zeros(resize_image(imutils.rotate_bound(ma, angle))) 
      cv2.imwrite(mas+f'{n}_y_{i}_rotated_{angle}.png',rotated)  
  return n
    
main = []
for n,(i,j) in enumerate(zip(images,masks)):
  print(n)
  main.append(augment(i,j,n))
  
# !pip install keras_segmentation

# Model Training  
import keras_segmentation

import shutil
img_list = np.array(os.listdir(org))
a = np.arange(550)
np.random.shuffle(a)
validation_data = img_list[a[:20]]

for i in validation_data:
  shutil.move(org+i, f'{path}/resized/augmented/val_original/'+i)
  shutil.move(mas+i, f'{path}/resized/augmented/val_mask/'+i)
  
  
model = keras_segmentation.models.unet.resnet50_unet(n_classes=3,  input_height=32*24, input_width=32*18,)

model.train(train_images =  org, 
            train_annotations = mas, 
            checkpoints_path = path+"resnet_unet_1" , 
            epochs=5, 
            steps_per_epoch=256, 
            batch_size=2, 
            validate=True,
            val_images = path+'resized/augmented/val_original/',
            val_annotations=path+'resized/augmented/val_mask')

import pickle
filename = f'{path}/finalized_model.sav'
pickle.dump(model, open(filename, 'wb'))
